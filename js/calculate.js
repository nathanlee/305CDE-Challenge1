/**
* Created with 305CDE-Challenge1.
* User: nathanlee
* Date: 2015-02-09
* Time: 09:06 AM
* To change this template use Tools | Templates.
*/

var operatorArray = ["+", "-", "*", "/"];

function pressButton(btnValue){

    var formula = document.getElementById('formula');
    var display = document.getElementById('display');
    var inputArray = ["0","1","2","3","4","5","6","7","8","9","+","-","*","/","=","del",".","c","q"];
    
    // To handle the forumla if undefined 
    if(!Boolean(formula.innerHTML)){
        formula.innerHTML = "0";
    }
    
    // To check the input value
    if (inputArray.indexOf(btnValue) == -1){
        alert("Invaild input element!");
        return false;
    }
    
    // To determine the action by pressed button
    if(btnValue == "del"){
        formula.innerHTML = formula.innerHTML.substring(0,(formula.innerHTML.length - 1));
    }else if(btnValue == "c"){
        formula.innerHTML = "";
        display.innerHTML = "";
    }else if(btnValue == "q"){
        if(display.innerHTML.length == 0){
            display.innerHTML = formula.innerHTML;
        }else{
            display.innerHTML = display.innerHTML + formula.innerHTML;
        }
        formula.innerHTML = "0";
    }else if(btnValue == "="){
        
        var error = false;
        var operatorIndex;
        var tempOperator = [];
        var result = 0;
        var singleOperator = true
        var i = 0;
        var temp;
        var checkingFormat;
        
        if(display.innerHTML.length != 0){
            if(formula.innerHTML != "0"){
                display.innerHTML = display.innerHTML + formula.innerHTML;
            }
            singleOperator = false;
            temp = display;
        }else{
            temp = formula;
        }
        
        // To handle the single operations
        while(temp.innerHTML[i] != null && singleOperator){
            
            if(temp.innerHTML[i] == "+" || temp.innerHTML[i] == "-" || 
               temp.innerHTML[i] == "*" || temp.innerHTML[i] == "/"){
                
                if(tempOperator.length == 0){
                    tempOperator.push(temp.innerHTML[i]);
                    operatorIndex = i;
                }else{
                    error = true;
                }
            }// End of if 
            i++;
        } 

        // To check the format of formula 
        checkingFormat = checkFormat(temp);
//         alert("checkFormat ="+checkingFormat);
        if(checkingFormat == true){
//             alert("111");
            error = true;
        }else{
//             alert("222");
//             alert("error = "+error);
            result = checkingFormat; 
        }
        
        // To handle the error
        if(error == true){
            formula.innerHTML = "ERROR";
            return false;
        }
        
        if(display.innerHTML.length != 0){
            display.innerHTML = "";
        }
        
        // To call the cal function to do the calculation
        formula.innerHTML = result;
        
    }else if(btnValue == "+" || btnValue == "-" || btnValue == "*" || btnValue == "/"){
        
        temp = formula.innerHTML[formula.innerHTML.length-1];
        
        // To check the previous character if operator
        if( temp != "+" && temp != "-" && temp != "*" && temp != "/" ){
            formula.innerHTML = addString(formula.innerHTML, btnValue);
        }
        
    }else{
        formula.innerHTML = addString(formula.innerHTML, btnValue);
    }
}

function addString(current, str){
   
    if(current.length == "1" && current == "0" && str != "." || current == "ERROR"){
        return current = str; 
    }else if(current.length == "0" && str == "."){
        return current = "0.";
    }else{
        return current = current + str;
    }
}

function cal(op1, op2, operator){
    switch(operator){
        case "+":
            return parseFloat(op1) + parseFloat(op2);
        case "-":
            return parseFloat(op1) - parseFloat(op2);
        case "*":
            return parseFloat(op1) * parseFloat(op2);
        case "/":
            return parseFloat(op1) / parseFloat(op2);
    }
}

function checkFormat(obj){
    var a = 0;
    var temp = obj.innerHTML;
    var tempIndex = 0;
    var tempArray = [];
    var result = 0;

     while(temp[a] != null){
//          alert(temp[a]);
         if (operatorArray.indexOf(temp[a]) != -1){
             tempIndex = operatorArray.indexOf(temp[a]);
//              alert("operatorArray = "+operatorArray.indexOf(temp[a]));
//              alert("tempIndex = "+tempIndex);
             if(tempArray.length == 0){
                 tempArray.push(temp.indexOf(operatorArray[tempIndex]));
             }else{
//                  alert("check point 2");
                 tempArray.push(temp.indexOf(operatorArray[tempIndex], (tempArray[tempArray.length -1]+1)));
             }
                 
//                  alert("tempObj = "+tempObj);
             
             
         }
         a++;
     }
//     alert("array "+tempArray);
    if(tempArray.length != 0){
        for(var i = 0; i < tempArray.length; i++){
            if(i == 0){
                result = temp.substring(0,tempArray[i]);
            }
            
            var operator = temp.substring(tempArray[i],tempArray[i]+1);
            var op2 = temp.substring(tempArray[i]+1,tempArray[i+1]);
            
//             alert("1 "+result);
//             alert("2 "+operator);
//             alert("3 "+op2);
            
            if(result.length == 0 || operator.length == 0 || op2.length == 0){
                return true;
            }
            result = cal(result,op2,operator);
            
        }
        return result;
    }
    return true;
}
